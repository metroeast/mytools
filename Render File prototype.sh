#!/bin/bash
## nuke Render Files on SAN
# prototype

## skipping
# Glenn Dyer
# John



## remove client render files
while read this_path
do
  case "$this_path" in
    *'/Glenn Dyer/'*)
      echo "__SKIPPING: $this_path"
      continue
      ;;
    *'/John/'*)
      echo "__SKIPPING: $this_path"
      continue
      ;;
  esac

  echo "Nuking Render: rm -rf '$this_path'/*"
  rm -rf "$this_path"/*
  
done < <(
#  ls -1d /Volumes/Island/Clients/*/*
  find -E /Volumes/Island/Clients/ -type d -regex '.+\.(fcpbundle|fcpcache).+/Render Files'
  find -E /Volumes/Island/Staff/ -type d -regex '.+\.(fcpbundle|fcpcache).+/Render Files'
)



## Size of Render Files, in GB
while read this_path
do
  du -d 0 -g "$this_path"/*
done < <(
  find -E /Volumes/Island/Staff/ -type d -regex '.+\.(fcpbundle|fcpcache).+/Render Files'
)

## OR…

find -E /Volumes/Island/Staff/ -type d -regex '.+\.(fcpbundle|fcpcache).+/Render Files' -exec du -d 0 -g '{}' \;




#  find -E /Volumes/Island/Clients/ -type d -regex '.+\.(fcpbundle|fcpcache).+/Render Files'
