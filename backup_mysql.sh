#!/bin/bash
#
# MySQL Dump with options
# data only or grants only, or both
# exclude root user from grants, or not
# specific databases, or all
#
# by IMU for MetroEast Community Media
# 


################
### simple usage string when run with no parameters
BASIC_USAGE="$0 [data] [grant] [noroot] [bases='db1 db2 db3 …'] [infix='short_db_name'] [outdir=/output/path] [outpre='filename_prefix'] [outsuf='filename_suffix'] (final params are mysql credential format similar to tools: mysql and mydsqldump)"
: ${1?"$BASIC_USAGE"}




#########
### Setup

# default output selection (alter with params: grant data)
DO_GRANT=0
DO_DATA=0


# grants criteria with or without root user (alter in place)
CRITERIA_WITH_ROOT="ORDER BY user = 'root' DESC, user, host"
CRITERIA_NO_ROOT="WHERE user != 'root' ORDER BY user, host"

# grants default criteria: with root user (alter with param: noroot)
GRANT_CRITERIA="$CRITERIA_WITH_ROOT"
grant_file_INFIX='ALL'


# databases to include, default is --all-databases (alter with param: bases)
INCLUDING_DATABASES='--all-databases'
data_file_INFIX='ALL'


# format a date for the filenames (alter in place)
file_date=$(date '+%Y-%m-%d--%H_%M_%S')


# output directory (alter with param: outdir)
OUT_DIRECTORY="$HOME"


# output filename prefix/suffix
file_PREFIX=''
file_SUFFIX=''



#############
### Functions

## minor mod to the usual grant dump
## we need to insert a flush after each grant to avoid complaints during import
function mygrants()
{
  # 1+: expecting MySQL credential parameters
  #
  # output raw grants SQL
  #

  while read grant_row
  do
    case $grant_row in
      ## notes for humans
      'Grants for '*)
        echo '##'
        echo "## $grant_row ##"
        continue
        ;;
      
      ## insert a flush after each grant
      *' IDENTIFIED BY PASSWORD '*)
        echo "$grant_row;"
        echo "FLUSH PRIVILEGES;"
        continue
        ;;
      
      ## regular grant row, add to output with termination
      *)
        echo "$grant_row;"
        ;;
        
    esac
  done < <(
    ## select the grants, format for import
    ## --batch (tab separator; each row a new line. non-tabular; escaping specials)
    ## --skip-column-names (no column names in results)
    mysql --batch --skip-column-names "$@" -e \
    "SELECT CONCAT('SHOW GRANTS FOR \'', user, '\'@\'', host, '\';') AS query FROM mysql.user $GRANT_CRITERIA" 2> /dev/null | \
    mysql "$@" 2> /dev/null
    
    exit_stat=$?
    [ $exit_stat -ne 0 ] && {
      echo "ERROR: mysql exit $exit_stat" 1>&2
    }
  )
}


# typical MySQL Dump, passing creds; option for databases selected
function mydata()
{
  # 1+: expecting MySQL credential parameters
  #
  # output direct to file from mysqldump
  #
  
  mysqldump "$@" \
--single-transaction \
--events \
--routines \
--triggers \
--result-file="$OUT_DIRECTORY/$data_file" \
$INCLUDING_DATABASES > /dev/null 2>&1

  exit_stat=$?
  [ $exit_stat -ne 0 ] && {
    echo "ERROR: mysqldump exit $exit_stat" 1>&2
    return $exit_stat
  }
}


# extract value from pair: variable="value" | var='val' | var=val
function value_from_pair ()
{
  # 1: var=val pair to parse
  # 2: var name to peel off
  #
  # output value portion without containing ' | "
  #
  
  this=${1#${2}=} ## remove var=
  this=${this#[\'|\"]} ## remove optional ' or " (start)
  this=${this%[\'|\"]} ## remove optional ' or " (end)
  
  echo "$this"
}




##############
### Parameters

# peel off parameters, until we reach '-' (assumed to be last; beginning of credential params)
while [[ "$1" != '' ]]
do
  case $1 in
    
    # help?
    '-h'|'--help'|'help')
      cat<<THIS
  $BASIC_USAGE

        grant               -->  export grants
        data                -->  export databases
        <neither or both>   -->  both grants and databases

        noroot              -->  skip root user grants (ignored when data only)
        <not present>       -->  include root user grants

        bases               -->  export specific databases (space delimited)
        <not present>       -->  export all databases
        bases='--all-databases' == <not present> (export all databases)
!!  NOTE: parameter 'bases' will not filter grants

        infix               -->  specify the data file name infix
        <not present>       -->  'ALL' or '+' delimited list of database names
!!  NOTE: parameter 'infix' must follow 'bases', or 'bases' will redefine infix
        
        outdir              -->  specify an output directory
        <not present>       -->  default: $OUT_DIRECTORY
        
        outpre              -->  file output prefix
        outsuf              -->  file output suffix


All Grants ONLY:
  $0 grant -u root -p


All Databases ONLY:
  $0 data -u root -p
-or-
  $0 data bases='--all-databases' -u root -p


All Grants and All Databases:
  $0 -u root -p


Grants (skipping root user) and All Databases
    with grant file name:  grant--noroot---$file_date.sql
    and data file name:    data--ALL---$file_date.sql

  $0 noroot -u root -p


All Grants and Databases: 'this', 'that' and 'other'
    with grant file name:  grant--ALL---$file_date.sql
    and data file name:    data--this+that+other---$file_date.sql

  $0 bases='this that other' -u root -p


All Grants and Databases: 'this', 'that' and 'other'
    with grant file name:  grant--ALL---$file_date.sql
    and data file name:    PRE-data--SPECIFIC---$file_date-SUF.sql
        
  $0 bases='this that other' infix='SPECIFIC' outpre='PRE-' outsuf='-SUF' -u root -p



NOTE: parameter 'bases' will not filter grants!
NOTE: parameter 'infix' must follow bases, or bases will supercede infix


THIS
      exit -13
      ;;
  
    # toggle grants ON
    'grant')
      DO_GRANT=1
      shift
      ;;
    
    # skip root grants
    'noroot')
      GRANT_CRITERIA="$CRITERIA_NO_ROOT"
      grant_file_INFIX='noroot'
      shift
      ;;
    
    # toggle data ON
    'data')
      DO_DATA=1
      shift
      ;;
    
    # specify databases
    'bases='*)
      bases=$(value_from_pair "$1" bases)
      case "$bases" in
        '--all-databases')
          # do nothing, use defaults: all databases
          ;;
        *)
          # do specified databases
          INCLUDING_DATABASES="--databases $bases"
          data_file_INFIX=$(echo "$bases" | sed 's/ /+/g')
          ;;
      esac
      shift
      ;;
    
    # specify data file name infix
    'infix='*)
      data_file_INFIX=$(value_from_pair "$1" infix)
      shift
      ;;
    
    # specify data file name infix
    'outpre='*)
      file_PREFIX=$(value_from_pair "$1" outpre)
      shift
      ;;
    
    # specify data file name infix
    'outsuf='*)
      file_SUFFIX=$(value_from_pair "$1" outsuf)
      shift
      ;;
    
    # specify output directory
    'outdir='*)
      outdir=$(value_from_pair "$1" outdir)
      OUT_DIRECTORY=${outdir%/}
      shift
      ;;
    
    # end of options, this should be the start of MySQL credentials
    -*)
      break
      ;;
    
    # not understood, but not fatal
    *)
      echo "? no grok ? --> $1"
      shift
      ;;
  esac
done




#######
## MAIN


# output files
grants_file="${file_PREFIX}grant--${grant_file_INFIX}---${file_date}${file_SUFFIX}.sql"
data_file="${file_PREFIX}data--${data_file_INFIX}---${file_date}${file_SUFFIX}.sql"



## if no export flag, then export all
[ $(echo $DO_GRANT + $DO_DATA | bc) == 0 ] && { DO_GRANT=1; DO_DATA=1; }


# do grants?
[ $DO_GRANT -eq 1 ] && {

  ## export mygrants to file
  mygrants "$@" > "$OUT_DIRECTORY/$grants_file"

  echo "Grants Done:"
  echo $'\t'"$OUT_DIRECTORY/$grants_file"
  echo $'\t'$(grep "^## Grants for " "$OUT_DIRECTORY/$grants_file" | wc -l) ": User+Host"
  echo $'\t'$(grep "^GRANT " "$OUT_DIRECTORY/$grants_file" | wc -l) ": Grants"
  echo $'\t'$(du -h "$OUT_DIRECTORY/$grants_file")
}


# do data?
[ $DO_DATA -eq 1 ] && {

  ## export the databases
  mydata "$@"

  echo "Databases Done:"
  echo $'\t'"$OUT_DIRECTORY/$data_file"
  echo $'\t'$(grep "^-- Current Database: " "$OUT_DIRECTORY/$data_file" | sort -u | wc -l)" : Databases"
  echo $'\t'$(grep "^-- Table structure for table " "$OUT_DIRECTORY/$data_file" | wc -l)" : Tables"
  echo $'\t'$(du -h "$OUT_DIRECTORY/$data_file")
}



exit 0
