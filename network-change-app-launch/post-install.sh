#!/bin/bash
#
#  post install script to activate the launch daemon
#

launchctl unload /Library/LaunchDaemons/org.metroeast.network-change-app-launch.plist
sleep 2
launchctl load -w /Library/LaunchDaemons/org.metroeast.network-change-app-launch.plist

exit
