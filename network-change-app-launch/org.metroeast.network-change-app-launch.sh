#!/bin/bash
#
#
#  decide if we are attached to the MetoEast network or not
#  if we are, we check for required apps, and launch them
#  we may mark them as launched, so we don't redo it
#  we may try to avoid reopening if when closed manually
#
#

#  1: any string passed will reset all semaphores
#


# how verbose
VERBOSE=2


#  root name of file (no ext) for related file naming
this=$(basename "$0")
filename_base=${this%.*}
echo "$filename_base"


# where we log this
log_file="/Library/Logs/${filename_base}.log"
echo "logging :: $log_file"


# where we store the semaphores
folder_semaphore=/tmp


# (inclusive)
earliest_hourmin=0900
latest_hourmin=2000


#  how long before we run this again…  required wait before running again
# 300 -> 5 min
wait_before_run_again=300


#  how long before we launch apps, again…  required wait before launching again
# 7200 -> 2 hours
wait_before_launch_again=7200


#  list of VPNs (when active, we're on the network)
facility_VPNs=$(
cat<<VPN_LIST
MetroEast Staff VPN
VPN_LIST
)


#  list of SSIDs (when connected to, we're on the network)
facility_SSIDs=$(
cat<<SSID_LIST
MetroEast-Staff
MetroEast-IT
MetroEast-Guest
SSID_LIST
)



#  list of Apps to launch
facility_Apps=$(
cat<<APP_LIST
Slack.app
APP_LIST
)



### FUNCTION PILE
##
#
#


function log_events {
  ## logging by stdout read line parsing
  #
  #  1: verbose level
  #
  #  0 or empty = normal messages
  #  1 = process
  #  2 = debug

  ## ensure we've something to test
  verb_lvl=${1:-0}
  VERBOSE=${VERBOSE:-0}
  
  ## read each line of redir stdout and push to log/terminal
  while read this_line
  do
    if [ "$verb_lvl" -le "$VERBOSE" ]
    then
      # echo "$now [$$] $this_line"
      now=$(date "+%Y-%m-%d_%H.%M.%S")
      echo "$now [$$] $this_line" >> "$log_file"
    fi
  done
}
## end function log_events



function semaphore_age {
  ##  return the access time for the given semaphore file
  ##  each file marks the last run time, by touch/access time
  ##  (last access date/time in seconds since Epoch)
  #
  #  1: the semaphore we're interested in
  #     the name of the app or empty string = app launch semaphore
  #  2: not empty = clear semaphore
  
  log_events 1 <<< "START semaphore_age: $1 $2"
  
  ## make the filename to check
  if [ "$1" == "" ]
  then
    sem_file="${filename_base}.run"
  else
    sem_file="${filename_base}--$1.run"
  fi
  
  log_events 2 <<< "sem_file: $folder_semaphore/$sem_file"
  
  
  ## stat it for access time
  #  let $(stat -s ${filename_base}.run)
  stat_cmd=$(stat -s "$folder_semaphore/$sem_file" 2> /dev/null | grep -oE 'st_atime=\d+')
  result=$?
  
  log_events 2 <<< "stat_cmd: $stat_cmd"
  
    
  ## when stat succeeds, we assign the var, else assume we need to create the file
  if [ $result -eq 0 ]
  then
    ## init the age variable, do the math
    let "$stat_cmd"
    now_sec=$(date "+%s")
    let "sem_age = now_sec - st_atime"
    
    log_events 2 <<< "sem_age = $now_sec - $st_atime"
    log_events 1 <<< "sem_age: $sem_age"

    echo "$sem_age"

  else
    ## assume age is zero
    log_events 1 <<< "sem_age: reset/new"
    
    echo "-1"

  fi
  
  
  
  return
}
## end function semaphore_age


function semaphore_remove {
  ##  remove semaphore file
  #
  #  1: the semaphore we're interested in
  #     the name of the app or empty string = app launch semaphore
  
  log_events 1 <<< "START semaphore_remove: $1"
  
  ## make the filename to check
  if [ "$1" == "" ]
  then
    sem_file="${filename_base}.run"
  else
    sem_file="${filename_base}--$1.run"
  fi
  
  log_events 2 <<< "sem_file: $folder_semaphore/$sem_file"

  
  ## are we clearing this
  rm "$folder_semaphore/$sem_file"
  result=$?
  
  log_events 1 <<< "rm'ing semaphore: $folder_semaphore/$sem_file"
  
  if [ $result -ne 0 ]
  then
    log_events <<< "cannot remove semaphore file: $folder_semaphore/$sem_file :: exiting"
    exit -13
  fi
  
  return

}
## end function semaphore_remove


function semaphore_touch {
  ##  touch semaphore file (set access time)
  #
  #  1: the semaphore we're interested in
  #     the name of the app or empty string = app launch semaphore
  
  log_events 1 <<< "START semaphore_touch: $1"
  
  ## make the filename to check
  if [ "$1" == "" ]
  then
    sem_file="${filename_base}.run"
  else
    sem_file="${filename_base}--$1.run"
  fi
  
  log_events 2 <<< "sem_file: $folder_semaphore/$sem_file"
  
  ## touch the file, reset the time since last checked/accessed
  touch "$folder_semaphore/$sem_file"
  result=$?
  
  log_events 1 <<< "touching: $folder_semaphore/$sem_file"
  
  if [ $result -eq 1 ]
  then
    log_events <<< "cannot update semaphore file: $folder_semaphore/$sem_file :: exiting"
    exit -13
  fi
  
  return
  
}
## end function semaphore_touch


###  Process function pile
function check_in_hours {
  ###  are we in "after hours" time…?  if so, end
  ###  when we aren't meant to be launching apps, we exit
  
  current_hourmin=$(date "+%H%m")
  log_events 2 <<< "current_hourmin: $current_hourmin"

  ## too early?
  log_events 2 <<< "earliest_hourmin: $earliest_hourmin"
  if [ "$earliest_hourmin" -gt "$current_hourmin" ]
  then
    log_events <<< "too early, exiting"
    exit 0
  fi

  ## too late?
  log_events 2 <<< "latest_hourmin: $latest_hourmin"
  if [ "$latest_hourmin" -lt "$current_hourmin" ]
  then
    log_events <<< "too late, exiting"
    exit 0
  fi

  log_events <<< "in hours"
  
  return

}
## end function check_in_hours


function check_recent_run {
  ###  have we run recently…?  if so, end
  ## have we waited long enough?
  
  log_events 1 <<< "time check: recent run more than $wait_before_run_again seconds"
  
  last_run_age=$(semaphore_age)
  if [[ "$last_run_age" -lt "$wait_before_run_again" && "$last_run_age" -ne -1 ]]
  then
    ## not long enough
    log_events <<< "too recent, exiting"
    exit 1
  fi
  
  log_events <<< "timely run"
  semaphore_touch
  
  return
  
}
## end function check_recent_run


function check_network {
  ##  we test for the VPN connection
  ##  assumes a single answer from scutil "Connected"
  
  raw_VPN=$(scutil --nc list | fgrep '(Connected)' | grep -oE '\".+?\"')
  raw_VPN=${raw_VPN#\"}
  current_VPN=${raw_VPN%\"}
  
  log_events 2 <<< "current_VPN: $current_VPN"

  grep -qE '^'"$current_VPN"'$' <<<"$facility_VPNs"
  is_VPN_connected=$?
  log_events 2 <<< "is_VPN_connected: $is_VPN_connected"

  ## we test for the WiFi SSID, currently connected
  
  raw_SSID=$(system_profiler SPAirPortDataType | grep -A 1 'Current Network Information:' | tail -n 1)
  raw_SSID=${raw_SSID##* }
  current_SSID=${raw_SSID%:}

  log_events 2 <<< "current_SSID: $current_SSID"

  grep -qE '^'"$current_SSID"'$' <<< "$facility_SSIDs"
  is_SSID_connected=$?
  log_events 2 <<< "is_SSID_connected: $is_SSID_connected"
  
  
  ## either being true, we are on the network, if not we exit
  if [[ $is_VPN_connected -eq 0 || $is_SSID_connected -eq 0 ]]
  then
    log_events <<< "on the network"
  else
    log_events <<< "not on the network, clearing semaphores, exiting"
    
    ## remove main semaphore
    semaphore_remove
    exit 1
  fi
  
  return
  
}
## end function check_network




####  BEGIN MAIN
###
##
#
#
#
#

log_events <<< "STARTING $0"


###  clear semaphores?
if [ $1 != "" ]
then
  ###  clear all semaphopres
  log_events < <(
    rm -v "$folder_semaphore/${filename_base}"* 2>&1
  )
  
  exit 0
fi


check_in_hours

check_recent_run

check_network


###  are the apps already launched…?   if so, next
###  have we recently launched the apps…?  if so, next
while read this_app
do
  log_events <<< "run check: $this_app"
  
  ## is it open already?
  pgrep_cmd=$(pgrep -f "$this_app")
  result=$?
  
  log_events 2 <<< "pgrep_cmd:"
  log_events 2 <<< "$pgrep_cmd"
  
  if [ $result -eq 0 ]
  then
    log_events <<< "found running: $this_app :: skipping"
    continue
  else
    log_events <<< "not found running: $this_app"
    
    ## have we opened it recently?
    last_launch_age=$(semaphore_age "$this_app")
    if [[ "$last_launch_age" -lt "$wait_before_launch_again" && "$last_launch_age" -ne -1 ]]
    then
      ## not long enough
      log_events <<< "too recent: $this_app :: skipping"
      continue
    fi
    
    ## open the app, in the background…  some apps don't seen to honor the background flag
    log_events 2 <<< "opening: $this_app"
    open -g -a "$this_app"
    semaphore_touch "$this_app"
  fi
  
done < <(
  echo "$facility_Apps"
)


###  DONE
log_events <<< "ENDING $0"

exit
