#!/bin/bash
#
# Basic stop and start of MySQL server

# Restart MySQL

# brew services based

## restart is failing, with a hung service
brew services restart mysql@5.6

#brew services stop mysql@5.6

## wait for the services to drop
#while [ $(ps -A | grep '/mysqld_safe ' | wc -l) -gt 1 ]; do sleep .5; echo -n .; done
#while [ $(ps -A | grep '/mysqld ' | wc -l) -gt 1 ]; do sleep .5; echo -n .; done

#sleep 3
#brew services start mysql@5.6



## wait for mysqld_safe
#while [ $(ps -A | grep '/mysqld_safe ' | wc -l) -lt 1 ]; do sleep .5; echo -n .; done

## and mysqld
#while [ $(ps -A | grep '/mysqld ' | wc -l) -lt 1 ]; do sleep .5; echo -n .; done

# manual install 5.6.x
#sudo /usr/local/mysql/support-files/mysql.server restart


## don't do this
#sudo /usr/local/mysql/support-files/mysql.server stop
#sleep 1
#sudo /usr/local/mysql/support-files/mysql.server start
