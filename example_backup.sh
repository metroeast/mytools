#!/bin/bash
#
# Simple example script to be called from your init/launchd/cron
#
# Premise:
#  Addresses the need to regularly create a full copy of websites for recovery,
#   migration, duplication, and so on.
#
# Requires scripts for features:
#   backup_mysql.sh  ::  for MySQL Dumps
#   burp_mysql.sh    ::  if you plan to restart mysql before/after the dump
#   drush            ::  for maintenance mode control
#   mysql & mysqldump ::  for the grants and dumps
#
# might need to add the path to the above tools and uncomment the following line
# export PATH="$PATH:/usr/local/bin:/usr/local/mysql/bin:/more/paths/here"
#
#
# Example Webserver:
#   site roots:
#               /Volumes/Sites/some.site.tld
#            -->  site--some.site.tld---[date].tar
#
#               /Volumes/Sites/another.site.tld
#            -->  site--another.site.tld---[date].tar
#
#   databases:
#               some.site.db another.db
#            -->  data--some.site.db+another.db---[date].sql
#
#               some.site2.db dependent.db also_depend.db
#            -->  data--some.site2.db+dependent.db+also_depend.db---[date].sql
#          -and-
#            -->  grant--noroot---[date].sql
#
#
# Drupal Shell items:
#  putting sites into maintenance mode before the backup
#  returning sites to normal mode after the backup
#  tests for drush and drupal presence, skips when not present
#
#
# Parameters:
#   pass your MySQL credentials as parameters
#   such as:  --user=root --password='root!password!here'
#


# move parameters / credentials to array
# this will probably break passwords with a space
cred=( "$@" )

## if you cannot supply your credentials via the cli call, then you could save them in the script, uncomment the following and edit as necessary.
# cred=( -uroot -p'my!password!here' )
# cred=( --user=root --password='my!password!here' )



# list of sites to process, one site path per line, site root directory is filename
# skips comments, empty lines and lines that start with whitespace
SITE_DIRS=$(cat <<LIST_HERE
# comments will be skipped
/Volumes/Sites/some.site.tld

# another comment
  /This/Will/Be/Skipped/skip.this.site
/Volumes/Sites/another.site.tld

LIST_HERE
)


# list of databases to process, unique file per line, space delimited for grouping
# skips comments, empty lines and lines that start with whitespace
DATABASES=$(cat <<LIST_HERE
# comments
some.site.db another.db

# more comments
some.site2.db dependent.db also_depend.db

# the following will create a file with all databases
--all-databases

LIST_HERE
)


# where to save output
OUTPUT_DIR="/path/to/output"


# filename prefix
file_PREFIX=''
#file_PREFIX="$HOSTNAME--"


# filename suffix
file_SUFFIX=''
#file_SUFFIX="$HOSTNAME--"


# verbosity: 1 = echo steps to stdout
VERBOSE=1


# restart the MySQL service?
restart_mysql_before_dump=0
restart_mysql_after_dump=1


# date format for reporting
date_format='+%Y-%m-%d  %H:%M:%S'


# init counter
prune_count=0


## function to prune files, keeping newest by count, using ls -t
function prune_this ()
{
  # 1: STRING:  file name prefix
  # 2: STRING:  file name suffix
  # 3: INT:  # > 0, the keep count, how many should we keep
  #                  when missing a default will be used
  #
  
  # ensure keep count is an INT
  keep_count=$(echo "$3" | sed 's/[^0-9]//g')
  
  # is this less than 1? use default
  [ $keep_count -lt 1 ] && keep_count=7
  
  # accum ttl prunes
  let "prune_count++"
  
  # basic report
  echo $(date "$date_format")" : Pruning #$prune_count : $1 * $2 : keeping:$keep_count"
  
  # count the files
  count=0
  remov=0
  while read this_item
  do
    # not a file, skip it
    [ ! -f "$this_item" ] && continue
    
    let "count++"
    [ $count -gt $keep_count ] && {
      # accum ttl removed
      let "remov++"

      # verbose report
      [ $VERBOSE -gt 0 ] && echo $(date "$date_format")" : Pruning #$prune_count : removing #$remov : $this_item"
      
      rm "$this_item"
    }
    
  done < <(
    ## newest first, list directory path, not contents
    ls -td "$OUTPUT_DIR/$1"*"$2"
  )
  
  # basic report
  echo $(date "$date_format")" : Done Pruning #$prune_count : found:$count, removed:$remov"
}


## function to loop site directory, setting maintenance mode per param 1
function sites_maint_toggle ()
{
  # 1: INT:  0 = off, 1 = on
  #
  
  ofn[0]='Off'
  ofn[1]='On'
  
  not=0
  drup=0
  ttl=0
  
  # basic report
  echo $(date "$date_format")" : Setting Maintenance Mode to ${ofn[$1]}"

  while read this_path
  do
    # skip comments, empty lines and lines that start with whitespace
    case "$this_path" in \#*|''|[[:space:]]*) continue;; esac
  
    # do settings change from the site root
    cd "$this_path"
    
    # accum ttl processed
    let "ttl++"
  
    # is site Drupal?
    drupal_site=$(drush status 2> /dev/null | grep 'Drupal version' > /dev/null; echo $?)
    
    # Drupal
    [ $drupal_site -eq 0 ] && {
      # accum Drupal processed
      let "drup++"

      # verbose report
      [ $VERBOSE -gt 0 ] && echo $(date "$date_format")" : maint_mode = $1 : $this_path"

      # toggle the setting
      drush vset maintenance_mode $1
    }
  
    # not Drupal
    [ $drupal_site -ne 0 ] && {
      # accum Drupal processed
      let "not++"
      
      # verbose report
      [ $VERBOSE -gt 0 ] && echo $(date "$date_format")" : Not Drupal : $this_path"
    }
  done < <(echo "$SITE_DIRS")
  
  # basic report
  echo $(date "$date_format")" : Setting Maintenance Mode to ${ofn[$1]} : ttl:$ttl, Drupal:$drup, Not:$not"
}



##############
###  MAIN  ###

# basic report, starting
echo $(date "$date_format")" : Beginning backup, MySQL and Website files"

# basic report, output/size
echo $(date "$date_format")" : Output Directory : "$(du -h -d 0 "$OUTPUT_DIR")

# date for all files generated
file_date=$(date '+%Y-%m-%d--%H_%M_%S')


# burp service before we start
burp_mysql.sh


# drush present?
drush_here=$(drush version > /dev/null 2>&1; echo $?)



## set maintenance mode(s) to on
[ $drush_here -eq 0 ] && {
  trap "{ echo '...exiting... cleaning up'; sites_maint_toggle 0; burp_mysql.sh; exit 255; }" TERM INT
  sites_maint_toggle 1
}



## loop site directory list, tar'ing directory to OUTPUT
count=0
echo $(date "$date_format")" : Tar Site Files"
while read this_path
do
  # skip comments, empty lines and lines that start with whitespace
  case "$this_path" in \#*|''|[[:space:]]*) continue;; esac
  
  # accum ttl processed
  let "count++"
  
  # name of site directory
  site_dir=$(basename "$this_path")
  
  # enclosing directory
  enc_dir=$(dirname "$this_path")
  
  # verbose report
  [ $VERBOSE -gt 0 ] && echo $(date "$date_format")" : tar #$count @ $enc_dir : $site_dir"
  
  # cd to enclosing directory
  cd "$enc_dir"
  
  # save site files to output directory
  output="${OUTPUT_DIR}/${file_PREFIX}site--${site_dir}---${file_date}${file_SUFFIX}.tar"
  tar -cf "$output" "./$site_dir"
  
  # … with gzip compression
  # tar -czf "$output.gz" "./$site_dir"
  
  # verbose report
  [ $VERBOSE -gt 0 ] && echo $(date "$date_format")" : tar #$count : $output"
done < <(echo "$SITE_DIRS")
echo $(date "$date_format")" : Tar Site Files : Complete : Sites:$count"



## loop databases list, MySQL Dump to OUTPUT
count=0
echo $(date "$date_format")" : MySQL Dump"
while read this_db_line
do
  # skip comments, empty lines and lines that start with whitespace
  case "$this_db_line" in \#*|''|[[:space:]]*) continue;; esac
  
  # accum ttl processed
  let "count++"
  
  # verbose report
  [ $VERBOSE -gt 0 ] && echo $(date "$date_format")" : backup_mysql #$count : $this_db_line"
  
  # restart the server, we've found this beneficial due to a memory leak
  [ $restart_mysql_before_dump -eq 1 ] && burp_mysql.sh
  
  # call backup tool with credentials (this would indicate the need to protect this script.)
  backup_mysql.sh data bases="$this_db_line" outdir="$OUTPUT_DIR" outpre="$file_PREFIX" outsuf="$file_SUFFIX" "${cred[@]}"
  
  # restart the server, we've found this beneficial due to a memory leak
  [ $restart_mysql_after_dump -eq 1 ] && burp_mysql.sh
done < <(echo "$DATABASES")
echo $(date "$date_format")" : MySQL Dump : Complete : Dumps:$count"



## do grants w/o root
backup_mysql.sh grant noroot outdir="$OUTPUT_DIR" outpre="$file_PREFIX" outsuf="$file_SUFFIX" "${cred[@]}"
# backup_mysql.sh grant outdir="$OUTPUT_DIR" outpre="$file_PREFIX" outsuf="$file_SUFFIX" "${cred[@]}"



## set maintenance mode(s) to off
[ $drush_here -eq 0 ] && {
  sites_maint_toggle 0
  trap TERM INT
}



# prune site files, keep the most recent 14
pattern_suffix="${file_SUFFIX}.tar"
#pattern_suffix="${file_SUFFIX}.tar.gz"
echo $(date "$date_format")" : Pruning ${file_PREFIX}site-- * $pattern_suffix"
while read this_path
do
  # skip comments, empty lines and lines that start with whitespace
  case "$this_path" in \#*|''|[[:space:]]*) continue;; esac
  
  # data file pattern, prefix / suffix
  pattern_prefix="${file_PREFIX}site--"$(basename "$this_path")
  
  # prune, keep most recent 14
  prune_this "$pattern_prefix" "$pattern_suffix" 14
done < <(echo "$SITE_DIRS")


## prune db files, keep the most recent 14
pattern_suffix="${file_SUFFIX}.sql"
echo $(date "$date_format")" : Pruning ${file_PREFIX}data-- * $pattern_suffix"
while read this_db_line
do
  # skip comments, empty lines and lines that start with whitespace
  case "$this_db_line" in \#*|''|[[:space:]]*) continue;; esac
  
  # is this --all-databases?
  [ "$this_db_line" == '--all-databases' ] && this_db_line='ALL'
  
  # data file pattern, prefix
  pattern_prefix="${file_PREFIX}data--"$(echo "$this_db_line" | sed 's/ /+/g')
  
  # prune, keep most recent 14
  prune_this "$pattern_prefix" "$pattern_suffix" 14
done < <(echo "$DATABASES")



# grant file pattern
# prune, keep most recent 14
prune_this "${file_PREFIX}grant--noroot" "${file_SUFFIX}.sql" 14
# prune_this "${file_PREFIX}grant--ALL" "${file_SUFFIX}.sql" 14



# basic report
echo $(date "$date_format")" : Completed Pruning $prune_count file sets"


# basic report, output/size
echo $(date "$date_format")" : Output Directory : "$(du -h -d 0 "$OUTPUT_DIR")


# burp service after we finish
burp_mysql.sh


# basic report, ending
echo $(date "$date_format")" : Done backing-up, MySQL and Website files"


# DONE
exit 0
